import { Component, OnInit } from '@angular/core';
declare let require:any;
 var s= require('../browser.js')
import { FormBuilder, Validators ,FormGroup,FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {NG_VALIDATORS} from '@angular/forms';
var client = require('ontology-dapi').client;
client.registerClient({});

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  public o_id;
  angForm: FormGroup;
  constructor(private router:Router,private spinner: NgxSpinnerService,private fb: FormBuilder) {
    this.createForm();
   }

   public id1: any;
   public id2: any;
 //   public account:string;
 //   public balance:number;
 //   public hashvalue;
 //   public a;
 
 
  public s1= 'L3duZU71BfrAHwNTffWQzSPrc2n9rVXnhx3As2Q9xVLSPb11vuTc';// particular acc WIF
 
  public privat5 = s.Crypto.PrivateKey.deserializeWIF(this.s1)   
  
  public from = new s.Crypto.Address('ANSdDAN2r2HNFawckYi12xXsZY2aNkyVdH')//('AXJFA9quEdQ4uZHv47Y5nCxiJfs5hAynYx')// b9d6d8c12ac9089d01a83eb8d419cde4136edd6f
  //Receiver's address
  public to = new s.Crypto.Address('AXJFA9quEdQ4uZHv47Y5nCxiJfs5hAynYx') //AXJFA9quEdQ4uZHv47Y5nCxiJfs5hAynYx,  AMa2ytW688ZVxLdbiH4qpP5DYfd4WckTsF
  //Amount to send
  public amount = 1
  //Asset type
  public assetType = 'ONT'
  //Gas price and gas limit are to compute the gas costs of the transaction.
  public gasPrice = '500';
  public gasLimit = '20000';
  //Payer's address to pay for the transaction gas
  public payer = this.from;
  public name='sandhiya';
      
 
 
 //  public pk_name:string;
 //  public pk_brand:string;
 //  public pk_quant: number;
 //  public pk_price: number;
 
 orderDetails()
    {
      this.spinner.show();
  
   //  console.log(this.pk_name,this.pk_brand,this.pk_quant,this.pk_price);
    const l2 = new s.Parameter('name', s.ParameterType.BigInteger,Number(this.o_id));
   
    
    
    const tx6 = s.TransactionBuilder.makeInvokeTransaction("p_details",[l2], 
        new s.Crypto.Address(s.utils.reverseHex('8e416524240e92d02bb41bb960c27e9d704f12c9')),
        
        String(this.gasPrice),
        String(this.gasLimit)
       
        );
       //  console.log("check",tx5);
       // 
        
    tx6.payer.value = this.from.value;
   //  console.log(this.from)
   //  console.log(this.privat5.key);
 
    const st2 = s.TransactionBuilder.signTransaction(tx6,this.privat5);
   //  console.log("signT",st1)
    
   //  console.log("Signed ", tx5);
    const rest8 = new s.RestClient('http://polaris1.ont.io:20334');
    rest8.sendRawTransaction(tx6.serialize()).then(res => {
       // console.log("add product",res[2])
 
   if(res['Desc'] == "SUCCESS")
   {
     this.spinner.hide();
     
     this.Product_Length();
   }
   else{
     this.spinner.hide();
   }
 
 
       })
       // this.Product_Length();
     }
 
 
     public Product_Length()
     {
       const rest10 = new s.RestClient('http://polaris1.ont.io:20334');
        
       var k;
       rest10.getStorage("8e416524240e92d02bb41bb960c27e9d704f12c9",'4f726465725f49645f4e756d626572').then(res2 =>{
         // console.log("Number of Products: ",res2)  
         // console.log(s.utils.hexstring2ab(res2.Result));
           
           k = s.utils.hexstring2ab(res2.Result);
           console.log("k",k);
 
 
         return this.ProductDetails(k);
           });
 
     }
 
     public myarray:any=[];
      
     public async  ProductDetails(m)
     {
       this.myarray.length =0;
       var tx7;
       var i;
       // console.log("Product Details");
       
       console.log(m[0]);
       for( i=1;i<=m[0];i++)
       {  
         try{
           var g1 = new s.Parameter('pk_id', s.ParameterType.BigInteger,(i));
           tx7 =   await client.api.smartContract.invokeRead( { scriptHash: "8e416524240e92d02bb41bb960c27e9d704f12c9",
           operation: "get_p_details",args: [g1] })
           
           tx7[3] = s.utils.hexstring2ab(tx7[3])
           tx7[4] = s.utils.hexstring2ab(tx7[4])
           tx7[0] = s.utils.hexstring2ab(tx7[0])
           tx7[1] = s.utils.hexstr2str(tx7[1])
           tx7[2] = s.utils.hexstr2str(tx7[2])
       
           tx7[3]= tx7[3][0];
           tx7[4]= tx7[4][0];
     
           this.myarray.push(tx7)
         }
         catch(e) {
        console.log(e)
         }
         finally{ }
       }
     
 
   //this.Product_Length();
       
     }
 
   ngOnInit() {
     this.Product_Length();
    
   }
   createForm() {
     this.angForm = this.fb.group({
      o_id: ['', Validators.required ],
        
     });
   }
 
   
 }























 




