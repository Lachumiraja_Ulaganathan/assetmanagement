import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from '../app.component';
import { AdminComponent } from '../admin/admin.component';

import { ProductComponent } from '../product/product.component';
import { UpdateProductComponent } from '../update-product/update-product.component';
import { CancelProductComponent } from '../cancel-product/cancel-product.component';
import { OrderDetailsComponent } from '../order-details/order-details.component';

// import { AuthguardGuard } from '../authguard.guard';
// import { CustomerGuard } from '../cutomer-guard/customer.guard';
// import { Web3servicesService } from '../services/web3services.service';


const routes: Routes = [
  { 
    path: '',
    redirectTo: 'admin',
    pathMatch: 'full'
  },

  { 
    path: 'admin',
    component: AdminComponent,
    // canActivate:[AuthguardGuard], 
     children:[
      {
        path: 'product',
        component: ProductComponent
      },
      { 
        path: 'update-product',
        component: UpdateProductComponent,
      },
      { 
        path: 'cancel-product',
        component: CancelProductComponent,
      },
      { 
        path: 'order-details',
        component: OrderDetailsComponent,
      }
    ]
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports:[RouterModule],
  declarations: [],
})
export class AppRoutingModule { }
