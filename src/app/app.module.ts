import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
// import { ReactiveFormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
// import { CustomerComponent } from './customer/customer.component';
import { ProductComponent } from './product/product.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { CancelProductComponent } from './cancel-product/cancel-product.component';
import { OrderDetailsComponent } from './order-details/order-details.component';

// import { AuthguardGuard } from './authguard.guard';
// import { CustomerGuard } from './cutomer-guard/customer.guard';
// import { Web3servicesService } from './services/web3services.service';


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,    
    ProductComponent,    
    UpdateProductComponent, CancelProductComponent, OrderDetailsComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot()
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [],//CustomerGuard
  
 
  bootstrap: [AppComponent]
})
export class AppModule { }
